<?php
namespace jui\autogrow;

use yii\web\AssetBundle;
/**
 * Autogrow Plugin class.
 * @author Moh Khoirul Anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class AutogrowPlugin extends AssetBundle
{
	public $sourcePath = '@jui/autogrow/assets';
	
	public $js = [
		'jquery.autosize.js',
	];
	
	public $depends = [
		'yii\web\JqueryAsset',
	];
}